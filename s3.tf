resource "aws_s3_bucket" "source_a" {
  bucket = "mph-source-bucket-a"
  acl = "private"

  tags = {
    Name        = "Bucket A - source"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket" "dest_b" {
  bucket = "mph-destination-bucket-b"
  acl = "private"

  tags = {
    Name        = "Bucket B - destination"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.source_a.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.exif_stripper_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".jpg"
  }

  depends_on = [aws_lambda_permission.allow_s3_events]
}

