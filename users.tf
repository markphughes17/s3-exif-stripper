resource "aws_iam_user" "usera" {
  name = "UserA"
  force_destroy = true
}

resource "aws_iam_user_policy" "usera_policy" {
  name = "useraPolicy"
  user = aws_iam_user.usera.name

  policy = file("iampolicies/iam-bucket-a.policy")
}

resource "aws_iam_user" "userb" {
  name = "UserB"
  force_destroy = true
}

resource "aws_iam_user_policy" "userB_policy" {
  name = "userbPolicy"
  user = aws_iam_user.userb.name

  policy = file("iampolicies/iam-bucket-b.policy")
}