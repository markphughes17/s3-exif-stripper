import pytest
from os import environ
import io
from PIL import Image

from handler import stripExif


def test_stripper():
  img = Image.open("src/Canon_40D.jpg")
  exif_data = img._getexif()
  #Confirm that the test image contains exif data
  assert len(exif_data) > 0
  stripped_image_raw = stripExif(img)
  stripped_image = Image.open(stripped_image_raw)
  new_exif_data = stripped_image._getexif()

  #confirm that the returned image contains no exif data
  assert new_exif_data == None