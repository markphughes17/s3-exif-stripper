import json
import urllib.parse
import boto3
import logging
from PIL import Image
from io import BytesIO
from os import environ

s3 = boto3.resource('s3')

def getImage(bucket, key):
  object = bucket.Object(key)
  response = object.get()

  file_stream = response["Body"]
  image = Image.open(file_stream)
  return image

def stripExif(image):
  data = list(image.getdata())
  image_without_exif = Image.new(image.mode, image.size)
  image_without_exif.putdata(data)
  file_stream  = BytesIO()
  image_without_exif.save(file_stream, format="jpeg")
  return file_stream

def uploadStrippedImage(file_stream, key, dest_bucket):
  bucket = s3.Bucket(dest_bucket)
  upload_response = bucket.put_object(Key=key, Body=file_stream.getvalue())
  return upload_response

def lambda_handler(event, context):
  #print event details
  logging.info("Input: ", event)

  # Get the object from the event and show its content type
  bucketName = event['Records'][0]['s3']['bucket']['name']
  bucket = s3.Bucket(bucketName)
  key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
  try:
    image = getImage(bucket, key)
  except Exception as e:
    print(e)
    print('Error getting object from bucket. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
    raise e
    
  try:
    file_stream = stripExif(image)
  except Exception as e:
    logging.error(e)
    logging.error("error stripping exif data from image")
    raise e
    
  
  dest_bucket = environ['dest_bucket']
  try:
    upload_response = uploadStrippedImage(file_stream, key, dest_bucket)
    print(upload_response)
  except Exception as e:
    logging.error(e)
    logging.error("Error uploading image to S3 destination")
    raise e

