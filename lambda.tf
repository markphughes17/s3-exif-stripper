
###Create the IAM role to attach to the lambda function
resource "aws_iam_role" "iam_for_lambda" {
    name = "lambda_iam_role"

    assume_role_policy = file("iampolicies/iamrole-lambda.policy")
    managed_policy_arns = [aws_iam_policy.readwriteS3policy.arn, data.aws_iam_policy.cloudwatch.arn]

}

resource "aws_iam_policy" "readwriteS3policy" {
    name = "s3-read-write-policy"

    policy = file("iampolicies/iamrole-s3.policy")
}


###Create the function
resource "aws_lambda_function" "exif_stripper_lambda" {
    function_name = var.function_name
    role          = aws_iam_role.iam_for_lambda.arn
    description   = var.function_desc
    filename      = data.archive_file.lambda_zip.output_path
    handler       = "handler.lambda_handler"
    source_code_hash = data.archive_file.lambda_zip.output_base64sha256
    runtime          = "python3.8"
    timeout          = "300"
    layers           = ["arn:aws:lambda:eu-west-2:770693421928:layer:Klayers-python38-Pillow:12"]

    environment {
        variables = {
            "dest_bucket" = aws_s3_bucket.dest_b.bucket
        }
  }


}

#create permission to allow S3 bucket to fire a lambda event
resource "aws_lambda_permission" "allow_s3_events" {
    statement_id  = "AllowExecutionFromS3Bucket"
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.exif_stripper_lambda.arn
    principal     = "s3.amazonaws.com"
    source_arn    = aws_s3_bucket.source_a.arn
}



data "aws_iam_policy" "cloudwatch" {
  name = "AWSLambdaBasicExecutionRole"
}

#zip the source code
data "archive_file" "lambda_zip" {
    type        = "zip"
    source_dir = "src/"
    output_path = "package.zip"
}