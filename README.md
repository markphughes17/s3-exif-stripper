# s3-exif-stripper

demo project of an event driven Lambda function that is written in PYthon3 to take an image uploaded to an S3 bucket (A) and save a copy with Exif data removed into another S3 Bucket (B).
The function will run every time a jpg image is uploaded to bucket A.

# Prerequisites
In order to deploy or run this application the following will be required.

- Terraform version 1.0+
- AWSCLI
- Python 3.8
- Pytest (only needed to run tests)

### To deploy AWS Resources
This instruction assumes you have an AWS account with access keys configured in your terminal.

- terraform apply --var-file vars/variables.tfvars

This creates the S3 buckets and deploys the python function to Lambda. 


### Usage
With the lambda function deployed in AWS, simply upload a jpg image to Bucket A, and then check Bucket B to see a copy of it with no exif data.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_archive"></a> [archive](#provider\_archive) | 2.2.0 |
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.52.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.readwriteS3policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.iam_for_lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_user.usera](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user.userb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_policy.userB_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy) | resource |
| [aws_iam_user_policy.usera_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy) | resource |
| [aws_lambda_function.exif_stripper_lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function) | resource |
| [aws_lambda_permission.allow_s3_events](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.dest_b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.source_a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_notification.bucket_notification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification) | resource |
| [archive_file.lambda_zip](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file) | data source |
| [aws_iam_policy.cloudwatch](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_function_desc"></a> [function\_desc](#input\_function\_desc) | Description of function | `string` | n/a | yes |
| <a name="input_function_name"></a> [function\_name](#input\_function\_name) | Name of lambda function | `string` | n/a | yes |

## Outputs

No outputs.


# Things I'd like to improve
- Add a notification in case of script failure, possibly using a dead letter queue to send events to an SNS topic. 
- Better protection against edge cases, e.g. limiting file size in case an image too big is uploaded that causes lambda execution to take so long it fails.

# Issues

- I had hoped to use moto and pytest to test the lambda itself but I had difficulties using the mocked AWS credentials to get the object from the mock S3 bucket, and wanted to avoid spending too much time so I opted to test the part of the function that strips the Exif data, as I can see in the AWS account that it is copying the image over, and manually downloading the image from Bucket B I can use a python environment to confirm the image has no exif data, but I wanted to at least have that tested properly.