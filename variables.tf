variable "function_name" {
    description = "Name of lambda function"
    type = string
}

variable "function_desc" {
    description = "Description of function"
    type = string
}

