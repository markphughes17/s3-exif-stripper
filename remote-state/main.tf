provider "aws" {
  region = "eu-west-2"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "mph-terraform-state"

  versioning {
    enabled = true
  }

}