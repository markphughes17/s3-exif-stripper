terraform {
    backend "s3" {
        bucket = "mph-terraform-state"
        key = "exif-stripper"
        region = "eu-west-2"
    }
}